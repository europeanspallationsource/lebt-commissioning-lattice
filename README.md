# LEBT-commissioning-lattice

Open XAL SMF lattice definition for the LEBT, with the configuration used for commissioning.

The main lattice file has been written by Natalia Milas, based on the official TraceWin lattice repository (https://bitbucket.org/europeanspallationsource/ess-lattice).